﻿
namespace c_sharp_essentials
{
    public class Point3D : Point
    {
        private int z;

        public Point3D(int x, int y, int z) : base(x, y)
        {
            this.z = z;
        }

        public void Move(int x, int y, int z)
        {
            Move(x, y);
            this.z = z;
        }

        public override void MoveToCenter()
        {
            base.MoveToCenter();
            z = 0;
        }

        public void SomeStrangeOperation()
        {
            int sum = x + y + z;
            Console.WriteLine($"sum: {sum}");

        }

        public override void GetPointLabel()
        {
            Console.WriteLine("This is Point3D");
        }
    }
}
