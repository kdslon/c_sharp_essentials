﻿namespace c_sharp_essentials
{
    public class Point
    {
        /* private -> dostęp tylko w klasie w której byt jest zdefiniowany
         * public -> dostęp globalny
         * protected -> dostęp w klasie zdefiniowanej i dziedziczących
         * internal
         * protected internal
         * private protected
         */

        protected int x; //0
        protected int y; //0

        public Point(int x, int y)
        {
            Move(x, y);
        }

        public int MoveXAndReturn(int x)
        {
            this.x = x;
            return this.x;
        }

        public void Move(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public virtual void MoveToCenter()
        {
            x = 0;
            y = 0;
        }

        public virtual void GetPointLabel()
        {
            Console.WriteLine("This is Point2D");
        }

        public void MoveX(int x)
        {
            this.x = x;
        }

        public void MoveY(int y)
        {
            this.y = y;
        }

        public void PrintPoint()
        {
            Console.WriteLine($"x: {x}\ny: {y}");
        }
    }
}
