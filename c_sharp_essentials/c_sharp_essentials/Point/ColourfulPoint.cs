﻿namespace c_sharp_essentials
{
    public enum Colour
    {
        Red,          //0
        Green,        //1
        Blue,         //2
        Yellow,       //3
        Purple,       //4
        Magenta,      //5
        Black = 555,  //555
        White         //556
    }


    public class ColourfulPoint : Point
    {
        private int colour;

        public ColourfulPoint(int x, int y, int colour) 
            : base(x, y)
        {
            this.colour = colour;
        }

        public void SetColour(int colour)
        {
            this.colour = colour;
        }

        public void SetColour(Colour colour)
        {
            this.colour = (int) colour;
        }

        public int GetColour()
        {
            return colour;
        }

        public Colour GetColourName()
        {
            return (Colour) colour;
        }
    }
}
