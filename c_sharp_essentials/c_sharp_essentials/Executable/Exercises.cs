﻿using c_sharp_essentials;
using c_sharp_essentials.Enemies;
using System.Text.Json.Serialization;

namespace c_sharp_essentials
{
    public static class Exercises
    {
        public static void IfElseSample()
        {
            int age = 20;
            Console.WriteLine("My age: " + age);

            //if else
            //true
            //false
            bool sample1 = age == 15;

            #region OPERACJE_LOGICZNE
            // age > 15;
            // 18 > 15
            // true

            //age < 15
            // 18 < 15
            // false

            //age == 15
            // 18 == 15
            // false


            // <
            // >
            // <=
            // >=
            // ==
            // != 
            #endregion

            #region ŁĄCZENIE OPERACJI LOGICZNYCH
            //ŁĄCZENIE OPERACJI LOGICZNYCH
            bool sample2 = age > 18 && age < 30;

            /*
                AND (&&) -> koniunkcja
                    X     Y    OUTPUT
                    0     0      0       
                    0     1      0
                    1     0      0
                    1     1      1
             */

            //age > 18 && age < 30
            //18 > 18 && 18 < 30
            //false(0)  &&  true(1)
            //false

            bool sample3 = age > 18 || age < 30;

            /*
                OR (||) -> alternatywa
                    X     Y    OUTPUT
                    0     0      0       
                    0     1      1
                    1     0      1
                    1     1      1
             */

            //age > 18 || age < 30
            //18 > 18 || 18 < 30
            //false(0)  ||  true(1)
            //true

            // ! -> NEGATION
            bool sample4 = !(age > 18) && age < 30;

            /*
               OR (||) -> alternatywa
                X  OUTPUT
                0    1       
                1    0
            */

            //!(age > 18) && age < 30;
            //!(18 > 18) && 18 < 30;
            //!false(0) && true(1)
            //true(1) && true(1)
            //true 
            #endregion

            if (age > 18) //tu mogę przekazać tylko true/false
            {
                Console.WriteLine("Can drink");
            }
            else
            {
                Console.WriteLine("Can't drink");
            }
        }

        public static void SwitchSample()
        {
            int x = 25;

            if (x == 15)
            {

            }
            else if (x == 16)
            {

            }
            else if (x == 17)
            {

            }
            else
            {

            }


            switch (x)
            {
                case 15:
                    //
                    //
                    //
                    //
                    //
                    //
                    break;
                case 16:
                    //
                    //
                    break;
                case 17:
                    break;
                default:
                    break;
            }

        }

        public static void Ex2_1()
        {
            int a = int.Parse(Console.ReadLine());
            int b = int.Parse(Console.ReadLine());
            int c = int.Parse(Console.ReadLine());

            bool isValid = (a * a + b * b) == c * c;

            if (isValid)
            {
                Console.WriteLine("Numbers are valid");
            }
            else
            {
                Console.WriteLine("Numbers are invalid");
            }
        }

        public static void Ex2_4()
        {
            float a;
            float b;
            float c;
            float x;

            Console.WriteLine("a: ");
            a = float.Parse(Console.ReadLine());

            if (a == 0)
            {
                Console.WriteLine("A variable 'a' cannot be equal to 0");
            }
            else
            {
                Console.WriteLine("b: ");
                b = float.Parse(Console.ReadLine());

                Console.WriteLine("c: ");
                c = float.Parse(Console.ReadLine());

                x = (c - b) / a;

                Console.WriteLine();
                //Console.WriteLine("a = {0}", a);
                Console.WriteLine($"a = {a:##.##}");
                Console.WriteLine("b = {0:##.##}", b);
                Console.WriteLine("c = {0:##.##}", c);
                Console.WriteLine("x = {0:##.##}", x);
            }
        }

        public static void RandomSample()
        {
            Random random = new Random();
            //int randomNumber = random.Next(101);
            //int randomNumber = random.Next();
            //int randomNumber = random.Next(1,3);

            float randomFloat = random.NextSingle();


            Console.WriteLine(randomFloat);
        }

        public static void Ex2_5()
        {
            Random r = new Random();
            int randomNumber;
            int userNumber;

            randomNumber = r.Next(0, 10);
            userNumber = int.Parse(Console.ReadLine());

            if (randomNumber == userNumber)
            {
                Console.WriteLine("Congratulations!");
            }
            else
            {
                Console.WriteLine($"Sorry, the number was {randomNumber}");
            }
        }

        public static void Loops()
        {
            int x = 0;

            //Warunek jest sprawdzany przed wykonaniem obrotu
            while (x < 10)
            {
                Console.WriteLine(x);
                x = x + 1;
            }


            //Warunek jest spawdzany po wykonaniu obrotu
            do
            {
                Console.WriteLine(x);
                x = x + 1;
            } while (x < 10);

            //i++ => i = i+1
            //++i => i = i+1
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(i);
            }

            //foreach
        }

        public static void Ex3_1()
        {
            int x;
            int y;

            for (x = 0; x <= 10; x++)
            {
                y = 3 * x;
                Console.WriteLine($"x = {x}\ty = {y}");
            }
        }

        public static void Ex3_5()
        {
            int i = 1;

            do
            {
                // i++ => najpierw zwraca wartość
                //a potem ją zwiększa
                //++i => najpierw zwiększa
                //potem zwraca

                Console.WriteLine(i++);
            } while (i <= 20);
        }

        public static void ModuloSample()
        {
            int x = 3 % 2; //reszta z dzielenia
            int y = 3 / 2; //dzielenie całkowite

            //3%2 => 1 i 1/2 => 1 i r. 1
            //3/2 => 1

            //3
            //0:3 -> 0
            //1:3 -> 1
            //2:3 -> 2
            //3:3 -> 0
            //4:3 -> 1 i 1/3 -> 1 i r. 1
            //5:3 -> 1 i 2/3 -> 1 i r. 2
            //6:3 -> 2 i 0/3 -> 2 i r. 0
        }

        public static void Ex3_10()
        {
            int sum = 0;
            for (int i = 1; i <= 100; ++i)
            {
                if (i % 2 == 0)
                {
                    sum = sum + i;
                }
            }
            Console.WriteLine($"Sum: {sum}");
        }

        public static void Ex3_18()
        {
            int sum = 0;
            int max;
            int min;
            int currentNumber;

            int n = 5;

            Random random = new Random();
            min = random.Next(0, 100);
            max = min;

            sum += min; //<=> sum = sum + min

            int iter = 1;

            Console.WriteLine($"{min}");

            while (iter < n)
            {
                currentNumber = random.Next(0, 100);
                Console.WriteLine($"{currentNumber}");

                if (max < currentNumber)
                    max = currentNumber;

                if (min > currentNumber)
                    min = currentNumber;

                sum += currentNumber;
                iter++;
            }

            //int x = 3/2 = 1.5 => 1
            // int i int => int
            // float i int => float

            Console.WriteLine($"Max: {max}");
            Console.WriteLine($"Min: {min}");
            Console.WriteLine($"Average: {(sum * 1.0f) / n}");
        }

        public static void Ex_1()
        {
            int numberA;
            int numberB;

            Console.WriteLine("Number A:");
            //"5" + "5" => "55"
            // "5x"
            numberA = int.Parse(Console.ReadLine());

            Console.WriteLine("Number B:");
            numberB = int.Parse(Console.ReadLine());

            for (int i = numberA; i <= numberB; ++i)
            {
                if (i % 2 == 0)
                {
                    Console.WriteLine(i);
                }
            }
        }

        public static void Ex_2()
        {
            Console.WriteLine("Length: ");
            int length = int.Parse(Console.ReadLine());
            string line = "";

            for (int i = 0; i < length; i++)
            {
                for (int j = 0; j < length; ++j)
                {
                    line += "*"; //=> "" + "*" => "*"
                                 //=> "*" + "*" => "**"
                }
                Console.WriteLine(line);
                line = "";
            }
        }

        public static void Ex_2a()
        {
            Console.WriteLine("Length: ");
            int length = int.Parse(Console.ReadLine());
            string line = "";

            for (int j = 0; j < length; ++j)
            {
                line += "*"; //=> "" + "*" => "*"
                             //=> "*" + "*" => "**"
            }

            for (int i = 0; i < length; i++)
            {
                Console.WriteLine(line);
            }
        }

        public static void CollectionsSample()
        {
            int[] intArray = new int[2];
            int sum = 0;

            for (int i = 0; i < intArray.Length; i++)
            {
                intArray[i] = 7;
            }

            foreach (int value in intArray)
            {
                sum += value;
            }
        }

        public static void Ex4_1()
        {
            int[] data = new int[10];
            for (int i = 0; i < data.Length; ++i)
            {
                data[i] = i;
                Console.WriteLine($"data[{i}] = {data[i]}");
            }
        }

        public static void Ex4_2()
        {
            int[] data = new int[10];
            for (int i = 0; i < data.Length; ++i)
            {               //9
                data[i] = data.Length - 1 - i;
                Console.WriteLine($"data[{i}] = {data[i]}");
            }
        }

        public static void Ex_2_tables()
        {
            int[] numbers = new int[100];
            Random random = new Random();

            for (int i = 0; i < numbers.Length; ++i)
            {
                numbers[i] = random.Next(1, 1000);
            }

            foreach (int num in numbers)
            {
                int count = 0;
                for (int i = 1; i <= num; ++i)
                {
                    if (num % i == 0)
                        count++;
                }

                if (count == 2)
                    Console.WriteLine(num);
            }
        }

        public static void TwoDimensionsTableSample()
        {
            int[] array = new int[10];

            int[,] array1 = new int[10, 10];

            int count = 0;

            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; ++j)
                {
                    array1[i, j] = count++;
                }
            }

            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; ++j)
                {
                    Console.Write(array1[i, j]);
                    Console.Write(" ");
                }
                Console.WriteLine("");
            }
        }

        public static void Ex4_3()
        {
            int n = 10;
            int sum = 0;
            int[,] matrix = new int[n, n];
            for (int i = 0; i < n; ++i)
            {
                for (int j = 0; j < n; ++j)
                {
                    if (i == j)
                    { 
                        matrix[i, j] = 1;
                        sum += matrix[i, j];
                    }
                    else
                        matrix[i, j] = 0;
                }
            }

            for (int i = 0; i < n; ++i)
            {
                for (int j = 0; j < n; ++j)
                {
                    Console.Write(matrix[i, j] + " ");          
                }
                Console.WriteLine();
            }
            Console.WriteLine("Sum: " + sum);
        }

        public static void Ex4_7()
        {
            int n = 10;
            int sum = 0;
            int[,] array = new int[n, n];

            for (int i = 0; i < n; ++i)
            {
                for (int j = 0; j < n; ++j)
                {
                    if (j == 0)
                    {
                        array[i, j] = i;
                        sum += array[i, j];
                    }
                    else if (j == 1)
                    {
                        array[i, j] = i * i;
                        sum += array[i, j];
                    }
                    else
                        array[i, j] = 0;
                }
            }

            for (int i = 0; i < n; ++i)
            {
                for (int j = 0; j < n; ++j)
                {
                    Console.Write(array[i, j] + " ");
                }
                Console.WriteLine();
            }
            Console.WriteLine("Sum: " + sum);
        }

        public static void ClassUseSample()
        {
            Point point = new Point(7, 5);
            point.PrintPoint();

            point.Move(200, 50);
            point.PrintPoint();

            Point3D point3D = new Point3D(0, 7, 5);
            
        }

        public static void Ex5_3()
        {
            Matrix matrix = new Matrix(10);
            matrix.GenerateMatrix();
            matrix.ProcessData();
            matrix.Print();
            Console.WriteLine("====");
            matrix = new Matrix(15);
            matrix.GenerateMatrix();
            matrix.ProcessData();
            matrix.Print();
            Console.WriteLine("====");
            matrix.GenerateMatrix();
            matrix.ProcessData();
            matrix.Print();
        }

        public static void Ex5_8()
        {
            Person person = new Person();
            person.ReadDataFromUser();
            person.Print();
        }

        public static void Ex5_9()
        {
            ExtendedPerson extendedPerson = new ExtendedPerson();
            extendedPerson.ReadDataFromUser();
            extendedPerson.Print();        
        }

        public static void Ex17_5()
        {
            ColourfulPoint point = new ColourfulPoint(2, 2, 2);

            point.SetColour(Colour.Green);

            Console.WriteLine(point.GetColour());
            Console.WriteLine(point.GetColourName().ToString());
        }

        public static void OverridingSample()
        {
            //Point point = new Point(1,2);
            //point.GetPointLabel();

            //Point5D point5D = new Point5D(0, 0, 0, 0, 0);
            //point5D.GetPointLabel();

            //Point point1 = new Point5D(1,1,1,1,1);
            //point1.GetPointLabel();

            Point point = new Point(0, 0);
            Point3D point3D = new Point3D(0, 0, 0);
            Point4D point4D = new Point4D(0, 0, 0, 0);
            Point5D point5D = new Point5D(0, 0, 0, 0, 0);

            Point[] points = new Point[4];
            points[0] = point;
            points[1] = point3D;
            points[2] = point4D;
            points[3] = point5D;

            for (int i = 0; i < points.Length; i++)
            {
                points[i].GetPointLabel();
            }

        }

        public static void Ex29_2()
        {
            Main main = new Main();
            Main inherited = new Inherited();

            main.PrintName();
            inherited.PrintName();

            Console.WriteLine(main.ToString());
            Console.WriteLine(inherited.ToString());
        }

        public static void AbstractClassSample()
        {
            Base baseClass = new Main();
        }

        public static void Ex30()
        {
            Shape rectangle = new Rectangle();
            rectangle.Draw();
            Console.WriteLine(rectangle.ToString());


            Shape triangle = new Triangle();
            triangle.Draw();
            Console.WriteLine(triangle.ToString());
        }

        public static void InterfaceSample()
        {
            Phone phone = new Phone();

            IChargeable chargeableDevice = phone;
            chargeableDevice.AddPower(15);
        }

        public static void Ex31_1()
        {
            IDrawable drawable = new Triangle();
            drawable.Draw2D();
            drawable.Draw3D();
        }

        public static void FightGame()
        {
            BaseEnemy orc 
                = new Orc(EnemyClass.Warrior,
                100, 20, 30);

            BaseEnemy undead = new Undead
                (EnemyClass.Hunter,
                200, 10, 20);

            FightSystem fightSystem = new FightSystem();
            fightSystem.StartFight(orc, undead);
            fightSystem.ProcessFight();
        }
    }   
}
