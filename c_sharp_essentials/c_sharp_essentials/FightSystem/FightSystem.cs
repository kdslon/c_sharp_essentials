﻿using c_sharp_essentials.Enemies;

namespace c_sharp_essentials
{
    public class FightSystem
    {
        private BaseEnemy attacker;
        private BaseEnemy defender;

        public void StartFight(BaseEnemy attacker, BaseEnemy defender)
        {
            this.attacker = attacker;
            this.defender = defender;
        }

        public void ProcessFight()
        {
            var attackerDamage = attacker.GetAttackPower();
            var defenderDamage = defender.GetAttackPower();
            ModifierSystem modifierSystem = new ModifierSystem();

            while (attacker.IsAlive()
                || defender.IsAlive())
            {

                Console.WriteLine("A DMG: " + attackerDamage);
                var sumOfMods = modifierSystem.ApplyAttackMods(attacker);
                Console.WriteLine("M(A) DMG: " + (attackerDamage + sumOfMods));

                defender.DealDamage(attackerDamage + sumOfMods);

                if (!defender.IsAlive())
                    break;

                Console.WriteLine("D DMG: " + defenderDamage);
                sumOfMods = modifierSystem.ApplyDefenderMods(defender);
                Console.WriteLine("M(D) DMG: " + (defenderDamage + sumOfMods));
                attacker.DealDamage(defenderDamage);
            }
            Console.WriteLine("Attacker: " + attacker.IsAlive());
            Console.WriteLine("Defender: " + defender.IsAlive());
        }
    }
}
