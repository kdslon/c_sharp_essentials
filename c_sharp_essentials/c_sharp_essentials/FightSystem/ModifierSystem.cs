﻿using c_sharp_essentials.Enemies;

namespace c_sharp_essentials
{
    public class ModifierSystem
    {
        public int ApplyAttackMods(BaseEnemy character)
        {
            int modValue = 0;
            if (character is IAttackModifier attMod)
            {
                modValue += attMod.GetAttackMod();
            }

            if (character is IClassModifier classMod)
            {
                modValue += classMod.GetClassMod();
            }
            return modValue;
        }

        public int ApplyDefenderMods(BaseEnemy character)
        {
            int modValue = 0;
            if (character is IDefenseModifier defMod)
            {
                modValue += defMod.GetDefenseMod();
            }

            if (character is IClassModifier classMod)
            {
                modValue += classMod.GetClassMod();
            }

            return modValue;
        }
    }
}
