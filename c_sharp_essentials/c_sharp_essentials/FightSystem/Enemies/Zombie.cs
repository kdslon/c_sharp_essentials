﻿namespace c_sharp_essentials.Enemies
{
    public class Zombie : BaseEnemy, IDefenseModifier
    {
        public Zombie(
            EnemyClass enemyClass,
            int health,
            int attackPower,
            int armour)
              : base(
              enemyClass,
              health,
              attackPower,
              armour)
        {
            //constructor
        }

        public int GetDefenseMod()
        {
            return 30;
        }
    }
}
