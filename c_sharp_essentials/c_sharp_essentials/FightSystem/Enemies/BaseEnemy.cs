﻿namespace c_sharp_essentials.Enemies
{
    public enum EnemyClass
    { 
        Warrior,
        Hunter,
        Mage
    }

    public abstract class BaseEnemy
    {
        protected int health;
        protected int armour;
        protected int attackPower;
        protected EnemyClass enemyClass;

        public BaseEnemy(
            EnemyClass enemyClass,
            int health,
            int attackPower,
            int armour)
        {
            this.health = health;
            this.attackPower = attackPower;
            this.armour = armour;
            this.enemyClass = enemyClass;
        }

        public bool IsAlive()
        {
            return health > 0;
        }

        public int GetAttackPower()
        {
            return attackPower;
        }

        public virtual void DealDamage(int dmg)
        {
            if (armour > 0)
            {
                armour -= dmg;
            }
            else
            {
                health -= dmg;
            }

            //alternative code
            //_ = armour > 0 ? armour -= dmg : health -= dmg;
        }
    }
}
