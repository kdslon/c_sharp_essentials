﻿namespace c_sharp_essentials.Enemies
{
    public class Undead : BaseEnemy, IClassModifier
    {
        public Undead(
            EnemyClass enemyClass,
            int health,
            int attackPower,
            int armour)
              : base(
              enemyClass,
              health,
              attackPower,
              armour)
        {
            //constructor
        }

        public int GetClassMod()
        {
            switch(enemyClass)
            {
                case EnemyClass.Warrior:
                    return 20;
                case EnemyClass.Hunter:
                    return 30;
                case EnemyClass.Mage:
                    return 10;
                default:
                    return 0;
            }
        }
    }
}
