﻿namespace c_sharp_essentials.Enemies
{
    public class Orc : BaseEnemy, IAttackModifier, IDefenseModifier
    {
        public Orc(
            EnemyClass enemyClass, 
            int health, 
            int attackPower, 
            int armour) 
            : base(
                  enemyClass, 
                  health, 
                  attackPower, 
                  armour)
        {
            //constructor
        }

        public int GetAttackMod()
        {
            return 50;
        }

        public int GetDefenseMod()
        {
            return 20;
        }
    }
}
