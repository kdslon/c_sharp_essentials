﻿namespace c_sharp_essentials.Enemies
{
    public interface IAttackModifier
    {
        int GetAttackMod();
    }

    public interface IDefenseModifier
    {
        int GetDefenseMod();
    }

    public interface IClassModifier
    {
        int GetClassMod();
    }
}
