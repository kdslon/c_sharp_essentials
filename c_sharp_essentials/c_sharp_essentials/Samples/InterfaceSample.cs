﻿namespace c_sharp_essentials
{
    public interface IChargeable
    {
        void AddPower(int voltage);
    }


    public class Phone : IChargeable
    {
        private int voltage;

        public void AddPower(int voltage)
        {
            this.voltage = voltage;
            Console.WriteLine(voltage);
        }

        public void PlayMusic()
        {

        }

        public void SendTextMessage()
        {

        }

        public void Call()
        { 
        
        }
    }
}
