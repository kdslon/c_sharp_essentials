﻿namespace c_sharp_essentials
{
    public class Matrix
    {
        private int[,] matrix;
        private int dimensionLength;

        public Matrix(int n)
        {
            dimensionLength = n;
            matrix = new int[dimensionLength, dimensionLength];
        }

        public void GenerateMatrix()
        {
            Random random = new Random();
                            //matrix.GetLength(0)
            for (int i = 0; i < dimensionLength; ++i)
            {                       //matrix.GetLength(1)
                for (int j = 0; j < dimensionLength; ++j)
                {
                    if (i == j)
                    {
                        matrix[i, j] = random.Next(0, 10);
                    }
                    else
                    {
                        matrix[i, j] = 0;
                    }
                }
            }
        }

        public void ProcessData()
        {
            int sum = 0;
            for (int i = 0; i < dimensionLength; ++i)
            {
                sum = sum + matrix[i, i];
            }

            Console.WriteLine($"Sum on the diagonal: {sum}");
        }

        public void Print()
        {
            for (int i = 0; i < dimensionLength; ++i)
            {
                for (int j = 0; j < dimensionLength; ++j)
                {
                    Console.Write(matrix[i, j] + " ");
                }
                Console.WriteLine();
            }
        }
    }
}
