﻿
namespace c_sharp_essentials
{
    public class Main : Base
    {
        public virtual void PrintName()
        {
            Console.WriteLine("I'm the object of the Main class");
        }

        public override string ToString()
        {
            return "I'm the object of the Main class";
        }

        public override void GetRandomNumber()
        {
           
        }
    }
}
