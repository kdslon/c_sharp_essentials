﻿namespace c_sharp_essentials
{
    public class Inherited : Main
    {
        public override void PrintName()
        {
            Console.WriteLine("I'm the object of the Inherited class");
        }

        public override string ToString()
        {
            return "I'm the object of the Inherited class";
        }
    }
}
