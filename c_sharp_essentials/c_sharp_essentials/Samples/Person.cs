﻿using System.Text;

namespace c_sharp_essentials
{
    public class Person
    {
        protected const string MISSING_MESSAGE = "<MISSING>";

        private string name;
        private string surname;
        private string street;
        private string postalCode;
        private string city;

        public Person()
        {
            name = MISSING_MESSAGE;
            surname = MISSING_MESSAGE;
            street = MISSING_MESSAGE;
            postalCode = MISSING_MESSAGE;
            city = MISSING_MESSAGE;
        }

        public Person(
            string name,
            string surname,
            string street,
            string postalCode,
            string city)
        {
            this.name = name;
            this.surname = surname;
            this.street = street;
            this.postalCode = postalCode;
            this.city = city;
        }

        public void ReadDataFromUser()
        {
            Console.Write("Name: ");
            name = Console.ReadLine();

            Console.Write("Surname: ");
            surname = Console.ReadLine();

            Console.Write("Street: ");
            street = Console.ReadLine();

            Console.Write("Postal Code: ");
            postalCode = Console.ReadLine();

            Console.Write("City: ");
            city = Console.ReadLine();

        }

        public void Print()
        {
            StringBuilder message = new StringBuilder();
            message.Append($"Name: {name}\n");
            message.Append($"Surname: {surname}\n");
            message.Append($"Street: {street}\n");
            message.Append($"Postal Code: {postalCode}\n");
            message.Append($"City: {city}");

            Console.WriteLine(message.ToString());
        }

    }
}
