﻿using System.Text;

namespace c_sharp_essentials
{
    public class ExtendedPerson : Person
    {
        private string education;
        private string position;

        public ExtendedPerson() : base()
        {
            education = MISSING_MESSAGE;
            position = MISSING_MESSAGE;
        }


        public ExtendedPerson(
            string name,
            string surname,
            string street,
            string postalCode,
            string city,
            string education,
            string position) : base(
                name,
                surname,
                street,
                postalCode,
                city)
        {
            this.education = education;
            this.position = position; 
        }

        public new void ReadDataFromUser()
        {
            base.ReadDataFromUser();

            Console.Write("Education: ");
            education = Console.ReadLine();

            Console.Write("Position: ");
            position = Console.ReadLine();
        }

        public new void Print()
        {
            base.Print();

            StringBuilder message = new StringBuilder();
            message.Append($"Education: {education}\n");
            message.Append($"Position: {position}\n");
            Console.WriteLine(message.ToString());
        }
    }
}
