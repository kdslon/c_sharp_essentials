﻿
namespace c_sharp_essentials
{
    public interface IDrawable
    {
        void Draw2D();
        void Draw3D();
    }
}
