﻿
namespace c_sharp_essentials
{
    public abstract class Shape : IDrawable
    {
        public abstract void Draw();

        public void Draw2D()
        {
            Console.WriteLine("DRAW 2D");
        }

        public void Draw3D()
        {
            Console.WriteLine("DRAW 3D");
        }

        public override string ToString()
        {
            return "Shape";
        }
    }

    public class Triangle : Shape
    {
        public override void Draw()
        {
            Console.WriteLine("I am a triangle");
        }
    }

    public class Rectangle : Shape
    {
        public override void Draw()
        {
            Console.WriteLine("I am a rectangle");
        }

        public override string ToString()
        {
            return "Rectangle type";
        }
    }
}
